import chrome from 'ui/chrome';
import { uiModules } from 'ui/modules';
import { chromeWrapper } from './services/chrome_wrapper';

import 'plugins/kibana/dashboard/dashboard_config';

let originalNavItemsVisibility = null;

function hideNavItems() {
  originalNavItemsVisibility = {};
  chromeWrapper.getNavLinks().forEach(navLink => {
    if (
      navLink.id !== 'kibana:dashboard' &&
      navLink.id !== 'opendistro-alerting' &&
      navLink.id !== 'security-multitenancy'
    ) {
      originalNavItemsVisibility[navLink.id] = navLink.hidden;
      chromeWrapper.hideNavLink(navLink.id, true, true);
    }
  });
}

export function Start($http) {
  const ROOT = chrome.getBasePath();
  const APP_ROOT = `${ROOT}`;
  const API_ROOT = `${APP_ROOT}/api/v1`;
  const GLOBAL_TENANT_VALUE = '';
  const PRIVATE_TENANT_VALUE = '__user__';

  const readOnlyConfig = chrome.getInjected('readonly_mode');

  let kibanaReadOnly = null;

  $http.get(`${API_ROOT}/auth/authinfo`).then(response => {
    response.data.roles.map(role => {
      if (role === 'kibana_read_only') {
        hideNavItems();
      }
    });
  });
}

uiModules.get('alerting').run(Start);
